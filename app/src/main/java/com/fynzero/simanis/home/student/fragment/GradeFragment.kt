package com.fynzero.simanis.home.student.fragment

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.fynzero.simanis.R
import com.fynzero.simanis.databinding.FragmentGradeBinding
import com.google.android.material.snackbar.Snackbar

class GradeFragment : Fragment() {
    private lateinit var binding: FragmentGradeBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentGradeBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.btnRekap.setOnClickListener {
            Snackbar.make(it, "Nilai berhasil direkap", Snackbar.LENGTH_LONG).show()
        }

        binding.btnLihat.setOnClickListener {
            val url =
                "https://docs.google.com/spreadsheets/d/1nU9U8u916Tv9noe9iMevlFF_nmW0Y5mcmjTSiAD1KN4/edit?usp=sharing"
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
            activity?.startActivity(intent)
        }
    }
}