package com.fynzero.simanis.home.teacher.fragment.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.fynzero.simanis.data.Student
import com.fynzero.simanis.databinding.GradeItemBinding

class GradeAdapter : RecyclerView.Adapter<GradeAdapter.ViewHolder>() {

    private val studentList = ArrayList<Student>()

    fun setStudent(list: ArrayList<Student>) {
        studentList.clear()
        studentList.addAll(list)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GradeAdapter.ViewHolder {
        val itemBinding =
            GradeItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: GradeAdapter.ViewHolder, position: Int) {
        holder.bind(studentList[position])
    }

    override fun getItemCount(): Int = studentList.size

    inner class ViewHolder(private val binding: GradeItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(student: Student) {
            with(binding) {
                txtNis.text = student.nis
            }
        }
    }

}
