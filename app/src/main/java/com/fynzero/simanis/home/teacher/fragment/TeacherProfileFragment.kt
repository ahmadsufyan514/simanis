package com.fynzero.simanis.home.teacher.fragment

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.fynzero.simanis.auth.login.LoginActivity
import com.fynzero.simanis.data.Teacher
import com.fynzero.simanis.databinding.FragmentTeacherProfileBinding
import com.google.firebase.database.*

class TeacherProfileFragment : Fragment() {

    private var _teacherProfileFragmentBinding: FragmentTeacherProfileBinding? = null
    private val binding get() = _teacherProfileFragmentBinding

    private lateinit var firebaseDatabase: FirebaseDatabase
    private lateinit var dbRef: DatabaseReference

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _teacherProfileFragmentBinding = FragmentTeacherProfileBinding.inflate(layoutInflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        firebaseDatabase = FirebaseDatabase.getInstance()
        dbRef = firebaseDatabase.getReference("Teacher")

        dbRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot.exists()) {
                    for (tcr in snapshot.children) {
                        val teacher = tcr.getValue(Teacher::class.java)
                        if (teacher != null) {
                            binding?.txtName?.text = teacher.username
                            binding?.txtNip?.text = teacher.nip
                            binding?.txtEmail?.text = teacher.email
                        }
                    }
                }
            }

            override fun onCancelled(error: DatabaseError) {
                Log.d("class_teacher", error.message)
            }
        })

        binding?.txtLogout?.setOnClickListener {
            startActivity(Intent(context, LoginActivity::class.java))
            activity?.finish()
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        _teacherProfileFragmentBinding = null
    }
}