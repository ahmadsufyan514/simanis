package com.fynzero.simanis.home.teacher.fragment.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.fynzero.simanis.data.ClassEntity
import com.fynzero.simanis.databinding.ClassItemBinding
import com.fynzero.simanis.home.teacher.detail.ClassTeacherDetailActivity

class ClassAdapter : RecyclerView.Adapter<ClassAdapter.ViewHolder>() {

    private val classList = ArrayList<ClassEntity>()

    inner class ViewHolder(private val binding: ClassItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(classEntity: ClassEntity) {
            with(binding) {
                txtClassName.text = classEntity.name
                txtClassDesc.text = classEntity.desc
                itemView.setOnClickListener {
                    val intent = Intent(itemView.context, ClassTeacherDetailActivity::class.java).apply {
                        putExtra(ClassTeacherDetailActivity.EXTRA_DETAIL, classEntity)
                    }
                    itemView.context.startActivity(intent)
                }
            }
        }
    }

    fun setClass(list: ArrayList<ClassEntity>) {
        classList.clear()
        classList.addAll(list)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemBinding = ClassItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(classList[position])
    }

    override fun getItemCount(): Int = classList.size
}