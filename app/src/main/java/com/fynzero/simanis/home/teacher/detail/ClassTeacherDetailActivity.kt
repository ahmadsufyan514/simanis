package com.fynzero.simanis.home.teacher.detail

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.fynzero.simanis.data.Task
import com.fynzero.simanis.databinding.ActivityClassTeacherDetailBinding
import com.fynzero.simanis.home.teacher.fragment.adapter.MessageAdapter
import com.google.firebase.database.*

class ClassTeacherDetailActivity : AppCompatActivity() {

    companion object {
        const val EXTRA_DETAIL = "extra_detail"
    }

    private lateinit var binding: ActivityClassTeacherDetailBinding

    private lateinit var firebaseDatabase: FirebaseDatabase
    private lateinit var dbRef: DatabaseReference

    private val messages = ArrayList<Task>()
    private val adapter = MessageAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityClassTeacherDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.rvMessage.layoutManager = LinearLayoutManager(this)
        binding.rvMessage.setHasFixedSize(true)
        adapter.notifyDataSetChanged()
        binding.rvMessage.adapter = adapter

        firebaseDatabase = FirebaseDatabase.getInstance()
        dbRef = firebaseDatabase.getReference("Task")

        binding.btnSend.setOnClickListener {
            sendMessage()
            startActivity(Intent(this, ClassTeacherDetailActivity::class.java))
            finish()
        }

        binding.icBack.setOnClickListener { finish() }

        dbRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot.exists()) {
                    for (msg in snapshot.children) {
                        val task = msg.getValue(Task::class.java)
                        if (task != null) {
                            messages.add(task)
                            adapter.setTask(messages)
                        }
                    }
                }
            }

            override fun onCancelled(error: DatabaseError) {
                Log.d("class_teacher", error.message)
            }
        })
    }

    private fun sendMessage() {
        val message = binding.edtTask.text.toString()

        val task = Task(message)
        val taskId = dbRef.push().key.toString()

        dbRef.child(taskId).setValue(task).addOnCompleteListener {
            Toast.makeText(this, "message sent", Toast.LENGTH_SHORT).show()
        }
    }
}