package com.fynzero.simanis.home.teacher.fragment.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.fynzero.simanis.data.Task
import com.fynzero.simanis.databinding.MessageItemBinding

class MessageAdapter : RecyclerView.Adapter<MessageAdapter.ViewHolder>() {

    private val tasks = ArrayList<Task>()

    inner class ViewHolder(private val binding: MessageItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(task: Task) {
            with(binding) {
                txtMessage.text = task.message
            }
        }
    }

    fun setTask(taskList: ArrayList<Task>) {
        tasks.clear()
        tasks.addAll(taskList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessageAdapter.ViewHolder {
        val itemBinding =
            MessageItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: MessageAdapter.ViewHolder, position: Int) {
        holder.bind(tasks[position])
    }

    override fun getItemCount(): Int = tasks.size
}