package com.fynzero.simanis.home.teacher.fragment

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.fynzero.simanis.R
import com.fynzero.simanis.data.ClassEntity
import com.fynzero.simanis.databinding.FragmentClassTeacherBinding
import com.fynzero.simanis.home.teacher.TeacherActivity
import com.fynzero.simanis.home.teacher.fragment.adapter.ClassAdapter
import com.google.firebase.database.*

class ClassTeacherFragment : Fragment() {

    private lateinit var binding: FragmentClassTeacherBinding
    private val classList = ArrayList<ClassEntity>()
    private val classAdapter = ClassAdapter()

    private lateinit var firebaseDatabase: FirebaseDatabase
    private lateinit var dbRef: DatabaseReference

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        binding = FragmentClassTeacherBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.rvClass.layoutManager = LinearLayoutManager(context)
        binding.rvClass.setHasFixedSize(true)
        classAdapter.notifyDataSetChanged()
        binding.rvClass.adapter = classAdapter

        firebaseDatabase = FirebaseDatabase.getInstance()
        dbRef = firebaseDatabase.getReference("Class")

        binding.txtCreateClass.setOnClickListener {
            addClassDialog()
        }

        dbRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot.exists()) {
                    for (cls in snapshot.children) {
                        val classEntity = cls.getValue(ClassEntity::class.java)
                        if (classEntity != null) {
                            binding.rvClass.visibility = View.VISIBLE
                            binding.txtNoActivity.visibility = View.GONE
                            binding.imgNoActivity.visibility = View.GONE
                            classList.add(classEntity)
                            classAdapter.setClass(classList)
                        }
                    }
                }
            }

            override fun onCancelled(error: DatabaseError) {
                Log.d("class_teacher", error.message)
            }
        })
    }

    private fun addClassDialog() {
        val dialog = Dialog(requireActivity())
        dialog.setContentView(R.layout.create_class_dialog)

        // init component
        val btnAdd = dialog.findViewById<TextView>(R.id.txt_add)
        val btnCancel = dialog.findViewById<TextView>(R.id.txt_cancel)
        val edtClassName = dialog.findViewById<EditText>(R.id.edt_class_name)
        val edtClassDesc = dialog.findViewById<EditText>(R.id.edt_class_desc)

        btnAdd.setOnClickListener {
            // create recycler view
            val name = edtClassName.text.toString()
            val desc = edtClassDesc.text.toString()

            val classEntity = ClassEntity(name, desc)
            val classId = dbRef.push().key.toString()

            dbRef.child(classId).setValue(classEntity).addOnCompleteListener {
                Toast.makeText(context, classEntity.name + " successfully created", Toast.LENGTH_SHORT).show()
            }

            startActivity(Intent(context, TeacherActivity::class.java))
            dialog.dismiss()
            activity?.finish()
        }

        btnCancel.setOnClickListener { dialog.dismiss() }

        dialog.show()
    }
}