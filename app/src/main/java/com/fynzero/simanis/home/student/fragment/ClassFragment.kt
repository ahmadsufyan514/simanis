package com.fynzero.simanis.home.student.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.fynzero.simanis.data.ClassEntity
import com.fynzero.simanis.databinding.FragmentClassBinding
import com.fynzero.simanis.home.teacher.fragment.adapter.ClassAdapter
import com.google.firebase.database.*

class ClassFragment : Fragment() {

    private var _fragmentClassBinding: FragmentClassBinding? = null
    private val binding get() = _fragmentClassBinding

    private val classList = ArrayList<ClassEntity>()
    private val classAdapter = ClassAdapter()

    private lateinit var firebaseDatabase: FirebaseDatabase
    private lateinit var dbRef: DatabaseReference

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _fragmentClassBinding = FragmentClassBinding.inflate(layoutInflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding?.rvClass?.layoutManager = LinearLayoutManager(context)
        binding?.rvClass?.setHasFixedSize(true)
        classAdapter.notifyDataSetChanged()
        binding?.rvClass?.adapter = classAdapter

        firebaseDatabase = FirebaseDatabase.getInstance()
        dbRef = firebaseDatabase.getReference("Class")

        dbRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot.exists()) {
                    for (cls in snapshot.children) {
                        val classEntity = cls.getValue(ClassEntity::class.java)
                        if (classEntity != null) {
                            binding?.rvClass?.visibility = View.VISIBLE
                            binding?.txtNoActivity?.visibility = View.GONE
                            binding?.imgNoActivity?.visibility = View.GONE
                            classList.add(classEntity)
                            classAdapter.setClass(classList)
                        }
                    }
                }
            }

            override fun onCancelled(error: DatabaseError) {
                Log.d("class_teacher", error.message)
            }
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _fragmentClassBinding = null
    }
}