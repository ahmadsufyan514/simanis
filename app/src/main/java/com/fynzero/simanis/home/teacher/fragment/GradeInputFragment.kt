package com.fynzero.simanis.home.teacher.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.fynzero.simanis.data.Student
import com.fynzero.simanis.databinding.FragmentGradeInputBinding
import com.fynzero.simanis.home.teacher.fragment.adapter.GradeAdapter
import com.google.firebase.database.*

class GradeInputFragment : Fragment() {

    private var _fragmentGradeInputBinding: FragmentGradeInputBinding? = null
    private val binding get() = _fragmentGradeInputBinding

    private lateinit var firebaseDatabase: FirebaseDatabase
    private lateinit var dbRef: DatabaseReference

    private val students = ArrayList<Student>()
    private val gradeAdapter = GradeAdapter()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _fragmentGradeInputBinding =
            FragmentGradeInputBinding.inflate(layoutInflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding?.rvStudent?.layoutManager = LinearLayoutManager(context)
        binding?.rvStudent?.setHasFixedSize(true)
        gradeAdapter.notifyDataSetChanged()
        binding?.rvStudent?.adapter = gradeAdapter

        firebaseDatabase = FirebaseDatabase.getInstance()
        dbRef = firebaseDatabase.getReference("Student")

        dbRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot.exists()) {
                    for (std in snapshot.children) {
                        val student = std.getValue(Student::class.java)
                        if (student != null) {
                            students.add(student)
                            gradeAdapter.setStudent(students)
                        }
                    }
                }
            }

            override fun onCancelled(error: DatabaseError) {
                Log.d("grade_input", error.message)
            }
        })

        binding?.btnGrade?.setOnClickListener {
            Toast.makeText(context, "Nilai terkirim!", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _fragmentGradeInputBinding = null
    }
}