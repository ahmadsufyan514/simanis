package com.fynzero.simanis.home.student.fragment

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.fynzero.simanis.auth.login.LoginActivity
import com.fynzero.simanis.data.Student
import com.fynzero.simanis.databinding.FragmentProfileBinding
import com.google.firebase.database.*

class ProfileFragment : Fragment() {

    private var _fragmentProfileBinding: FragmentProfileBinding? = null
    private val binding get() = _fragmentProfileBinding

    private lateinit var firebaseDatabase: FirebaseDatabase
    private lateinit var dbRef: DatabaseReference

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _fragmentProfileBinding = FragmentProfileBinding.inflate(layoutInflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        firebaseDatabase = FirebaseDatabase.getInstance()
        dbRef = firebaseDatabase.getReference("Student")

        dbRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot.exists()) {
                    for (std in snapshot.children) {
                        val student = std.getValue(Student::class.java)
                        if (student != null) {
                            binding?.txtName?.text = student.username
                            binding?.txtNip?.text = student.nis
                            binding?.txtEmail?.text = student.email
                        }
                    }
                }
            }

            override fun onCancelled(error: DatabaseError) {
                Log.d("class_student", error.message)
            }
        })

        binding?.txtLogout?.setOnClickListener {
            startActivity(Intent(context, LoginActivity::class.java))
            activity?.finish()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _fragmentProfileBinding = null
    }
}