package com.fynzero.simanis.data

data class Teacher(
        var nip: String = "",
        var username: String = "",
        var email: String? = "",
        var password: String? = ""
)
