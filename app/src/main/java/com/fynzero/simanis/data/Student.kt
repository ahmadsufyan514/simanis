package com.fynzero.simanis.data

data class Student(
    var nis: String = "",
    var username: String = "",
    var email: String? = "",
    var password: String? = ""
)
