package com.fynzero.simanis.auth.register

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.fynzero.simanis.auth.login.LoginAsStudentActivity
import com.fynzero.simanis.data.Student
import com.fynzero.simanis.databinding.ActivityRegisterStudentBinding
import com.google.firebase.database.*

class RegisterStudentActivity : AppCompatActivity() {

    private lateinit var binding: ActivityRegisterStudentBinding

    // database
    private lateinit var firebaseDatabase: FirebaseDatabase
    private lateinit var dbRef: DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegisterStudentBinding.inflate(layoutInflater)
        setContentView(binding.root)

        firebaseDatabase = FirebaseDatabase.getInstance()
        dbRef = firebaseDatabase.getReference("Student")

        binding.btnSignUp.setOnClickListener {
            val nis = binding.edtNis.text.toString().trim()
            val username = binding.edtUserName.text.toString().trim()
            val email = binding.edtEmail.text.toString().trim()
            val password = binding.edtPassword.text.toString().trim()

            when {
                nis.isEmpty() -> {
                    binding.edtNis.error = "Kolom ini harus di isi"
                    binding.edtNis.requestFocus()
                }
                username.isEmpty() -> {
                    binding.edtUserName.error = "Kolom ini harus di isi"
                    binding.edtUserName.requestFocus()
                }
                email.isEmpty() -> {
                    binding.edtEmail.error = "Kolom ini harus di isi"
                    binding.edtEmail.requestFocus()
                }
                password.isEmpty() -> {
                    binding.edtPassword.error = "Kolom ini harus di isi"
                    binding.edtPassword.requestFocus()
                }
                else -> {
                    saveStudent(nis, username, email, password)
                }
            }
        }

        binding.icBack.setOnClickListener { finish() }
    }

    private fun saveStudent(nis: String, username: String, email: String, password: String) {
        val student = Student()
        student.nis = nis
        student.username = username
        student.email = email
        student.password = password

        checkingNis(nis, student)
    }

    private fun checkingNis(nis: String, dataStudent: Student) {
        dbRef.child(nis).addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                val student = snapshot.getValue(Student::class.java)
                if (student == null) {
                    // store data
                    dbRef.child(nis).setValue(dataStudent)
                    Toast.makeText(
                        this@RegisterStudentActivity,
                        "pendaftaran berhasil!",
                        Toast.LENGTH_SHORT
                    ).show()
                    startActivity(
                        Intent(
                            this@RegisterStudentActivity,
                            LoginAsStudentActivity::class.java
                        )
                    )
                } else {
                    Toast.makeText(
                        this@RegisterStudentActivity,
                        "user sudah digunakan!",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }

            override fun onCancelled(error: DatabaseError) {
                Toast.makeText(this@RegisterStudentActivity, error.message, Toast.LENGTH_SHORT)
                    .show()
            }
        })
    }
}