package com.fynzero.simanis.auth.login

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.fynzero.simanis.data.Teacher
import com.fynzero.simanis.databinding.ActivityLoginAsTeacherBinding
import com.fynzero.simanis.home.teacher.TeacherActivity
import com.google.firebase.database.*

class LoginAsTeacherActivity : AppCompatActivity() {

    private lateinit var binding: ActivityLoginAsTeacherBinding
    private lateinit var dbRef: DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginAsTeacherBinding.inflate(layoutInflater)
        setContentView(binding.root)

        dbRef = FirebaseDatabase.getInstance().getReference("Teacher")

        binding.btnLoginGuru.setOnClickListener {
            val nip = binding.edtNip.text.toString().trim()
            val password = binding.edtPassword.text.toString().trim()

            when {
                nip.isEmpty() -> {
                    binding.edtNip.error = "NIP tidak boleh kosong"
                    binding.edtNip.requestFocus()
                }
                password.isEmpty() -> {
                    binding.edtPassword.error = "password tidak boleh kosong"
                    binding.edtPassword.requestFocus()
                }
                else -> {
                    pushLogin(nip, password)
                }
            }
        }

        binding.icBack.setOnClickListener { finish() }
    }

    private fun pushLogin(nip: String, password: String) {
        dbRef.child(nip).addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                val teacher = snapshot.getValue(Teacher::class.java)
                if (teacher == null) {
                    Toast.makeText(
                        this@LoginAsTeacherActivity,
                        "Guru tidak ditemukan!",
                        Toast.LENGTH_SHORT
                    ).show()
                } else {
                    if (teacher.password == password) {
                        val intent =
                            Intent(this@LoginAsTeacherActivity, TeacherActivity::class.java)
                        startActivity(intent)
                        Toast.makeText(
                            this@LoginAsTeacherActivity,
                            "berhasil login!",
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        Toast.makeText(
                            this@LoginAsTeacherActivity,
                            "password anda salah!",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            }

            override fun onCancelled(error: DatabaseError) {
                Toast.makeText(
                    this@LoginAsTeacherActivity,
                    "guru tidak ditemukan!",
                    Toast.LENGTH_SHORT
                ).show()
            }

        })
    }
}