package com.fynzero.simanis.auth.register

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.fynzero.simanis.auth.login.LoginAsTeacherActivity
import com.fynzero.simanis.data.Teacher
import com.fynzero.simanis.databinding.ActivityRegisterTeacherBinding
import com.google.firebase.database.*

class RegisterTeacherActivity : AppCompatActivity() {

    private lateinit var binding: ActivityRegisterTeacherBinding

    // database
    private lateinit var firebaseDatabase: FirebaseDatabase
    private lateinit var dbRef: DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegisterTeacherBinding.inflate(layoutInflater)
        setContentView(binding.root)

        firebaseDatabase = FirebaseDatabase.getInstance()
        dbRef = firebaseDatabase.getReference("Teacher")

        binding.btnSignUp.setOnClickListener {
            val nip = binding.edtNip.text.toString().trim()
            val username = binding.edtUserName.text.toString().trim()
            val email = binding.edtEmail.text.toString().trim()
            val password = binding.edtPassword.text.toString().trim()

            when {
                nip.isEmpty() -> {
                    binding.edtNip.error = "Kolom ini harus di isi"
                    binding.edtNip.requestFocus()
                }
                username.isEmpty() -> {
                    binding.edtUserName.error = "Kolom ini harus di isi"
                    binding.edtUserName.requestFocus()
                }
                email.isEmpty() -> {
                    binding.edtEmail.error = "Kolom ini harus di isi"
                    binding.edtEmail.requestFocus()
                }
                password.isEmpty() -> {
                    binding.edtPassword.error = "Kolom ini harus di isi"
                    binding.edtPassword.requestFocus()
                }
                else -> {
                    saveTeacher(nip, username, email, password)
                }
            }
        }

        binding.icBack.setOnClickListener { finish() }
    }

    private fun saveTeacher(nip: String, username: String, email: String, password: String) {
        val teacher = Teacher()
        teacher.nip = nip
        teacher.username = username
        teacher.email = email
        teacher.password = password

        checkingNip(nip, teacher)
    }

    private fun checkingNip(nip: String, dataTeacher: Teacher) {
        dbRef.child(nip).addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                val teacher = snapshot.getValue(Teacher::class.java)
                if (teacher == null) {
                    // store data
                    dbRef.child(nip).setValue(dataTeacher)
                    Toast.makeText(
                        this@RegisterTeacherActivity,
                        "pendaftaran berhasil!",
                        Toast.LENGTH_SHORT
                    ).show()
                    startActivity(
                        Intent(
                            this@RegisterTeacherActivity,
                            LoginAsTeacherActivity::class.java
                        )
                    )
                } else {
                    Toast.makeText(
                        this@RegisterTeacherActivity,
                        "user ini sudah digunakan!",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }

            override fun onCancelled(error: DatabaseError) {
                Toast.makeText(this@RegisterTeacherActivity, error.message, Toast.LENGTH_SHORT)
                    .show()
            }
        })
    }
}