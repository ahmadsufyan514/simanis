package com.fynzero.simanis.auth.login

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.fynzero.simanis.data.Student
import com.fynzero.simanis.databinding.ActivityLoginAsStudentBinding
import com.fynzero.simanis.home.student.StudentActivity
import com.google.firebase.database.*

class LoginAsStudentActivity : AppCompatActivity() {

    private lateinit var binding: ActivityLoginAsStudentBinding
    private lateinit var dbRef: DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginAsStudentBinding.inflate(layoutInflater)
        setContentView(binding.root)

        dbRef = FirebaseDatabase.getInstance().getReference("Student")

        binding.btnLoginSiswa.setOnClickListener {
            val nis = binding.edtNis.text.toString().trim()
            val password = binding.edtPassword.text.toString().trim()

            when {
                nis.isEmpty() -> {
                    binding.edtNis.error = "NIS tidak boleh kosong"
                    binding.edtNis.requestFocus()
                }
                password.isEmpty() -> {
                    binding.edtPassword.error = "password tidak boleh kosong"
                    binding.edtPassword.requestFocus()
                }
                else -> {
                    pushLogin(nis, password)
                }
            }
        }

        binding.icBack.setOnClickListener { finish() }
    }

    private fun pushLogin(nis: String, password: String) {
        dbRef.child(nis).addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                val student = snapshot.getValue(Student::class.java)
                if (student == null) {
                    Toast.makeText(
                        this@LoginAsStudentActivity,
                        "siswa tidak ditemukan",
                        Toast.LENGTH_SHORT
                    ).show()
                } else {
                    if (student.password == password) {
                        val intent =
                            Intent(this@LoginAsStudentActivity, StudentActivity::class.java)
                        startActivity(intent)
                        Toast.makeText(
                            this@LoginAsStudentActivity,
                            "berhasil login!",
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        Toast.makeText(
                            this@LoginAsStudentActivity,
                            "password anda salah!",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            }

            override fun onCancelled(error: DatabaseError) {
                Toast.makeText(
                    this@LoginAsStudentActivity,
                    "siswa tidak ditemukan",
                    Toast.LENGTH_SHORT
                ).show()
            }

        })
    }
}