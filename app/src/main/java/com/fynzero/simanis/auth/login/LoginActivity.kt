package com.fynzero.simanis.auth.login

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import coil.load
import coil.transform.RoundedCornersTransformation
import com.fynzero.simanis.R
import com.fynzero.simanis.auth.register.RegisterStudentActivity
import com.fynzero.simanis.auth.register.RegisterTeacherActivity
import com.fynzero.simanis.databinding.ActivityLoginBinding

class LoginActivity : AppCompatActivity() {

    private lateinit var binding: ActivityLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.imgLogin.load(R.drawable.login_image) {
            crossfade(true)
            crossfade(2000)
            transformations(RoundedCornersTransformation(12f))
        }

        binding.btnLoginGuru.setOnClickListener {
            startActivity(Intent(this, LoginAsTeacherActivity::class.java))
        }

        binding.btnLoginSiswa.setOnClickListener {
            startActivity(Intent(this, LoginAsStudentActivity::class.java))
        }

        binding.txtTeacher.setOnClickListener {
            startActivity(Intent(this, RegisterTeacherActivity::class.java))
        }

        binding.txtStudent.setOnClickListener {
            startActivity(Intent(this, RegisterStudentActivity::class.java))
        }
    }
}