package com.fynzero.simanis.data

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class ClassEntity(
        var name: String = "",
        var code: String = "",
        var desc: String =  "",
): Parcelable
